SRC_DIR = src
BLD_DIR = classes
DOC_DIR = docs
EXT_DIR = ext
TEST_DIR = test
JUNIT = junit-4.10.jar
PROJ_NAME = TP1
TEST_BUILD = testEtat17.jar

.PHONY : test

### Compilation
all: $(patsubst $(SRC_DIR)/%.java,$(BLD_DIR)/%.class,$(wildcard $(SRC_DIR)/**/*.java))

$(BLD_DIR)/%.class: $(BLD_DIR) $(SRC_DIR)/%.java
	javac -cp $(SRC_DIR) $(word 2,$^) -d $(word 1,$^)

$(BLD_DIR):
	mkdir -p $(BLD_DIR)

### Test ###
test: $(patsubst $(TEST_DIR)/%.java,run/%,$(wildcard $(TEST_DIR)/**/*.java))
	make clean

$(TEST_DIR)/$(BLD_DIR)/%.class: $(TEST_DIR)/$(BLD_DIR) $(TEST_DIR)/%.java
	javac -cp $(SRC_DIR):$(TEST_DIR):$(EXT_DIR)/$(JUNIT):$(EXT_DIR)/$(TEST_BUILD) $(word 2,$^) -d $(word 1,$^)

run/%: $(TEST_DIR)/$(BLD_DIR)/%.class
	java -cp $(TEST_DIR)/$(BLD_DIR):$(EXT_DIR)/$(JUNIT):$(EXT_DIR)/$(TEST_BUILD) org.junit.runner.JUnitCore $(subst /,.,$(subst run/,,$@))

$(TEST_DIR)/$(BLD_DIR):
	mkdir -p $@

### Jar ###
$(JAR_NAME): manifest-ex all doc
	cd $(BLD_DIR); jar cvfm ../$(JAR_NAME) ../manifest-ex $(PROJ_NAME) -C .. $(DOC_DIR) -C .. $(SRC_DIR) -C .. $(TEST_DIR)

### Main ###

### Doc ###
doc:
	mkdir -p $(DOC_DIR);cd $(SRC_DIR);javadoc -author -d ../$(DOC_DIR) -subpackages $(PROJ_NAME)

### Clean ###
clean:
	rm -rf $(BLD_DIR)
	rm -rf $(DOC_DIR)
	rm -rf $(TEST_DIR)/$(BLD_DIR)