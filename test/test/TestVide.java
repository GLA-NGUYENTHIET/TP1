package test;

import testEtat.*;
import org.junit.*;
import static org.junit.Assert.*;

public class TestVide {

	private Conteneur C;
	private boolean unmod_taille, unmod_cap;
	private int old_taille, old_cap;

	// Creation d'un conteneur vide
	@Before
	public void creerConteneurVide() {
		try {
			C = new Conteneur(10);
		} catch (Exception e) {
			fail();
		}
		this.unmod_cap = true;
		this.unmod_taille = true;
		this.old_cap = 10;
		this.old_taille = 0;
	}

	// Test de non modif
	@After
	public void end() {
		try {
			assertNotNull(this.C);
			if (unmod_cap) {
				assertTrue(this.C.capacite() == this.old_cap);
			}
			if (unmod_taille) {
				assertTrue(this.C.taille() == this.old_taille);
			}
		} catch (Exception e) {
			fail();
		}
	}

	// Objectif de test : remise a zero d'un conteneur vide
	// Resultat attendu : remise a zero impossible, levee de l'exception
	// ErreurConteneur
	@Test
	public void razVide() {
		try {
			C.raz();
			fail();
			// on force le test a echouer si aucune exception n'est levee
		} catch (ErreurConteneur e) {
			// si une exception de type ErreurConteneur est levee, le test reussit
			// on verifie que le conteneur n'a pas ete modifie
			assertEquals(0, C.taille());
			assertEquals(10, C.capacite());
			assertTrue(C.estVide());
		} catch (Exception e) {
			fail();
			// si une exception d'un autre type est levee, le test echoue
		}
	}

	// Objectif : ajout, on ne devrait pas avoir de probleme particulier comme dans
	// les deux autres cas
	// attendu : 0 exception, taille++, nouvel élément
	@Test
	public void ajouter() {
		try {
			Object o = new Object();
			this.C.ajouter(o, o);
			this.unmod_taille = false;
			assertTrue(this.C.taille() == 1);
			assertFalse(this.C.estVide());
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : retirer
	// attente : rien ne se passe et pas de modification
	@Test
	public void retirer() {
		try {
			Object o = new Object();
			this.C.retirer(o);
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : redimensionner
	// attente : exception
	@Test
	public void redimensionner() {
		try {
			this.C.redimensionner(12);
			fail();
		} catch (ErreurConteneur e) {
			;
		}

		try {
			this.C.redimensionner(5);
			fail();
		} catch (ErreurConteneur e) {
			;
		}
	}

	// objectif : tester le fonctionnement de present
	// attente : false
	@Test
	public void present() {
		try {
			Object o = new Object();
			assertFalse(this.C.present(o));
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : tester valeur
	// attente : conteneur vide donc il y aura une exception
	@Test
	public void valeur() {
		try {
			Object o = new Object();
			this.C.valeur(o);
			fail();
		} catch (ErreurConteneur e) {
			;
		}
	}

	// objectif : tester estvide
	// attente : true
	@Test
	public void estVide() {
		try {
			assertTrue(this.C.estVide());
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : tester taille
	// attente : 0
	@Test
	public void taille() {
		try {
			assertTrue(this.C.taille() == 0);
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : tester capacite
	// attente : 10
	@Test
	public void capacite() {
		try {
			assertTrue(this.C.capacite() == 10);
		} catch (Exception e) {
			fail();
		}
	}
}
