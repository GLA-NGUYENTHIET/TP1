package test;

import testEtat.*;
import org.junit.*;
import static org.junit.Assert.*;

public class TestPlein {

	private Conteneur C;
	private Object A1, A2, A3, A4, A5, B1, B2, B3, B4, B5;
	private int old_cap, old_taille;
	private boolean unmod_cap, unmod_taille, unmod_A1, unmod_A2, unmod_A3, unmod_A4, unmod_A5;

	// Creation d'un conteneur plein
	@Before
	public void creerConteneurPlein() {
		try {
			C = new Conteneur(5);
		} catch (Exception e) {
			fail();
		}
		A1 = new Object();
		A2 = new Object();
		A3 = new Object();
		A4 = new Object();
		A5 = new Object();
		B1 = new Object();
		B2 = new Object();
		B3 = new Object();
		B4 = new Object();
		B5 = new Object();
		try {
			C.ajouter(A1, B1);
			C.ajouter(A2, B2);
			C.ajouter(A3, B3);
			C.ajouter(A4, B4);
			C.ajouter(A5, B5);
		} catch (Exception e) {
			fail();
		}
		this.old_cap = this.C.capacite();
		this.old_taille = this.C.taille();
		this.unmod_A1 = true;
		this.unmod_A2 = true;
		this.unmod_A3 = true;
		this.unmod_A4 = true;
		this.unmod_A5 = true;
		this.unmod_cap = true;
		this.unmod_taille = true;
	}

	// tests de non modif
	@After
	public void end() {
		try {
			if (this.unmod_A1) {
				assertSame(this.B1, this.C.valeur(A1));
			}
			if (this.unmod_A2) {
				assertSame(this.B2, this.C.valeur(A2));
			}
			if (this.unmod_A3) {
				assertSame(this.B3, this.C.valeur(A3));
			}
			if (this.unmod_A4) {
				assertSame(this.B4, this.C.valeur(A4));
			}
			if (this.unmod_A5) {
				assertSame(this.B5, this.C.valeur(A5));
			}
			if (this.unmod_cap) {
				assertTrue(this.C.capacite() == this.old_cap);
			}
			if (this.unmod_taille) {
				assertTrue(this.C.taille() == this.old_taille);
			}
			assertNotNull(this.C);
			assertNotNull(this.A1);
			assertNotNull(this.A2);
			assertNotNull(this.A3);
			assertNotNull(this.A4);
			assertNotNull(this.A5);
		} catch (Exception e) {
			fail();
		}
	}

	// Objectif de test : ajout d'un element dont la cle est deja presente dans un
	// conteneur plein
	// Resultat attendu : ajout possible et ancien couple de meme cle ecrase
	@Test
	public void ajouterPresentPlein() {

		Object B = new Object();
		try {
			C.ajouter(A2, B);
		} catch (Exception e) {
			fail();
		}

		assertTrue(C.present(A2));
		try {
			assertEquals(B, C.valeur(A2));
		} catch (Exception e) {
			fail();
		}
		assertEquals(5, C.taille());
		assertEquals(5, C.capacite());
		this.unmod_A2 = false;
	}

	// objectif : ajouter un element non présent
	// attendu : débordement excepton
	@Test
	public void ajouterNonPresent() {
		try {

			Object A6 = new Object();
			Object B6 = new Object();
			try {
				this.C.ajouter(A6, B6);
				fail();
			} catch (DebordementConteneur e) {
				;
			}
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : redimensionner
	// attendu : vu que le conteneur est rempli, pas d'exception
	@Test
	public void redimensionner() {
		try {
			this.C.redimensionner(6);
			assertTrue(this.C.capacite() == this.old_cap + 1);
			this.unmod_cap = false;
			Object o = new Object();
			this.C.ajouter(o, o);
			assertTrue(this.C.present(o));
			assertSame(o, this.C.valeur(o));
			assertTrue(this.C.taille() == this.old_taille + 1);
			this.unmod_taille = false;
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : redimensionner avec une valeur illégale
	// attendu : exception parce qu'on ne peut pas remettre une capacité inférieure
	// a la capacite actuelle
	@Test
	public void redimensionnerIllegal() {
		try {
			this.C.redimensionner(0);
		} catch (ErreurConteneur e) {
			;
		}
	}

	// objectif : deux redimension de suite
	// attendu : ok pour la premiere mais la 2e declenche exception
	@Test
	public void deuxRedimension() {
		try {
			this.C.redimensionner(10);
			assertTrue(this.C.capacite() == 10);
			this.unmod_cap = false;
			try {
				this.C.redimensionner(20);
			} catch (ErreurConteneur e) {
				;
			}

			try {
				this.C.redimensionner(5);
			} catch (ErreurConteneur e) {
				;
			}

		} catch (Exception e) {
			fail();
		}
	}

	// objectif : retirer conteneur plein
	// attendu : on ne peut plus redimensionner
	@Test
	public void retirer() {
		try {
			this.C.retirer(A1);
			this.unmod_A1 = false;
			this.unmod_taille = false;
			assertTrue(this.C.taille() == this.old_taille - 1);
			try {
				this.C.redimensionner(10);
				fail();
			} catch (ErreurConteneur e) {
				;
			}

			try {
				this.C.valeur(A1);
			} catch (ErreurConteneur e) {
				;
			}
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : retirer non present, meme comportement que non vide
	// attente : pas de pb, pas de modif
	@Test
	public void retirerNonPresent() {
		try {
			Object o = new Object();
			this.C.retirer(o);
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : raz
	// attendu : on ne peut plus redimensionner, on peut pas faire deux appel a raz
	// a la suite
	@Test
	public void raz() {
		try {
			this.C.raz();
			assertTrue(this.C.taille() == 0);

			try {
				this.C.valeur(A1);
				fail();
			} catch (ErreurConteneur e) {
				;
			}
			try {
				this.C.valeur(A2);
				fail();
			} catch (ErreurConteneur e) {
				;
			}

			try {
				this.C.valeur(A3);
				fail();
			} catch (ErreurConteneur e) {
				;
			}

			try {
				this.C.valeur(A4);
				fail();
			} catch (ErreurConteneur e) {
				;
			}

			try {
				this.C.valeur(A5);
				fail();
			} catch (ErreurConteneur e) {
				;
			}

			this.unmod_taille = false;
			this.unmod_A1 = false;
			this.unmod_A2 = false;
			this.unmod_A3 = false;
			this.unmod_A4 = false;
			this.unmod_A5 = false;
			try {
				this.C.raz();
			} catch (ErreurConteneur e) {
				;
			}
		} catch (Exception e) {
			fail();
		}
	}

	// obecjtif : verifier bon fonctionnement de présent
	// attendu : vrai quand element est dedans, faux quand contraire
	@Test
	public void present() {
		try {
			assertTrue(this.C.present(A1));
			assertTrue(this.C.present(A2));
			assertTrue(this.C.present(A3));
			assertTrue(this.C.present(A4));
			assertTrue(this.C.present(A5));
			Object o = new Object();
			assertFalse(this.C.present(o));
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : vérifier bon fonctionnement de valeur
	// attendu : les bonnes associations, puis exception si absent
	@Test
	public void valeur() {
		try {
			assertSame(this.C.valeur(A1), B1);
			assertSame(this.C.valeur(A2), B2);
			assertSame(this.C.valeur(A3), B3);
			assertSame(this.C.valeur(A4), B4);
			assertSame(this.C.valeur(A5), B5);

			Object o = new Object();
			try {
				this.C.valeur(o);
			} catch (ErreurConteneur e) {
				;
			}
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : un conteneur plein n'est pas vide
	// attente : faux
	@Test
	public void estVide() {
		try {
			assertFalse(this.C.estVide());
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : tester la taille qui doit etre de 5 et qu'un appel a celui ci ne
	// fait pas de modification
	// attente : 5
	@Test
	public void taille() {
		try {
			assertTrue(this.C.taille() == 5);
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : tester la capacité qui doit etre égale a 5 et qui ne modifie rien
	// attente : 5
	@Test
	public void capacite() {
		try {
			assertTrue(this.C.capacite() == 5);
		} catch (Exception e) {
			fail();
		}
	}

}
