package test;

import testEtat.*;
import org.junit.*;
import static org.junit.Assert.*;
import java.util.Random;

public class TestInit {

	private Conteneur C;
	private final Random r = new Random();

	// Objectif de test : creation d'un conteneur de capacite > 1
	// Resultat attendu : conteneur vide cree de la capacite passee en argument
	@Test
	public void capaciteSup1() {
		try {
			C = new Conteneur(5);
		} catch (Exception e) {
			fail();
			// on force le test a echouer si une exception est levee
		}
		assertNotNull(C);
		assertEquals(0, C.taille());
		assertEquals(5, C.capacite());
		assertTrue(C.estVide());
	}

	// objectif : création d'un conteneur de capacité 1 et valeur au hasard en
	// dessous de 1
	// attendu : exception
	@Test
	public void capaciteInfOuEgal1() {

		try {
			C = new Conteneur(r.nextInt(-100) - 1);
			fail();
		} catch (Exception e) {
			assertNull(C);
		}

		try {
			C = new Conteneur(1);
			fail();
		} catch (Exception e) {
			assertNull(C);
		}

	}
}
