package test;

import testEtat.*;
import org.junit.*;
import static org.junit.Assert.*;

public class TestNonVide {

	private Conteneur C;
	private Object A1, A2, B1, B2;
	private int old_cap, old_taille;
	// ces booleans servent à savoir si nos elements ont été modifiés, et si il ne
	// le sont pas, il faut verifier qu'ils représentent la meme chose
	private boolean unmod_cap;
	private boolean unmod_taille;
	private boolean unmod_A1;
	private boolean unmod_A2;

	// Creation d'un conteneur partiellement rempli
	@Before
	public void creerConteneurNonVide() {
		try {
			C = new Conteneur(5);
			A1 = new Object();
			A2 = new Object();
			B1 = new Object();
			B2 = new Object();
			C.ajouter(A1, B1);
			C.ajouter(A2, B2);
			this.old_cap = this.C.capacite();
			this.old_taille = this.C.taille();
			this.unmod_cap = true;
			this.unmod_taille = true;
			this.unmod_A1 = true;
			this.unmod_A2 = true;
			assertNotNull(this.C);
			assertNotNull(this.A1);
			assertNotNull(this.A2);
			assertNotNull(this.B1);
			assertNotNull(this.B2);
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : ajouter, retirer, evaluer un entier, flottant
	// attente : pas d'exception grace à l'auto conversion de java
	@Test
	public void operationPrimitif() {
		try {
			int n = 10;
			double f = 3.14;
			C.ajouter(n, f);
			assertTrue(this.C.taille() == this.old_taille + 1);
			this.unmod_taille = false;
			assertEquals(f, this.C.valeur(n));
			assertTrue(this.C.present(n));
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : tenter de redimensioner le conteneur avec des valeurs légales et
	// illégales
	// attente : 2 exception, et que la capacité n'a pas été modifiée
	@Test
	public void redimensionnerNonVideEtNonPlein() {
		try {
			try {
				this.C.redimensionner(0);
				fail();
			} catch (ErreurConteneur e) {
				;
			}

			try {
				this.C.redimensionner(10);
				fail();
			} catch (ErreurConteneur e) {
				;
			}

		} catch (Exception e) {
			fail();
		}
	}

	// objectif : ajouter un couple dont la valeur est déja liée à une autre clé,
	// vérifier que le couple est present, et vérifier que
	// la valeur associée est la meme
	// attente : pas d'exception et que la nouvelle clé soit présente
	@Test
	public void ajouterDeuxCleUneValeur() {
		try {
			Object A3 = new Object();
			C.ajouter(A3, this.B1);
			assertTrue(C.present(A3));
			assertSame(C.valeur(A3), this.B1);
			assertTrue(this.C.taille() == (this.old_taille + 1));
			this.unmod_taille = false;
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void ajouterBasique() {
		try {
			Object A3 = new Object();
			Object B3 = new Object();
			this.C.ajouter(A3, B3);
			assertTrue(this.C.present(A3));
			assertSame(this.C.valeur(A3), B3);
			assertTrue(this.C.taille() == (this.old_taille + 1));
			this.unmod_taille = false;
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : vérifier qu'un ecrasement de valeur est bien effectué, la taille
	// n'est pas modifié
	// attendu : l'ancienne clé pointe vers la nouvelle valeur
	@Test
	public void ajouterEcraser() {
		try {
			Object B3 = new Object();
			this.C.ajouter(A1, B3);
			assertNotSame(this.C.valeur(A1), this.B1);
			assertSame(this.C.valeur(A1), B3);
			this.unmod_A1 = false;
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : ajouter 2 clé qui pointe sur la meme valeur, écrase l'une des clés, vérifier que l'autre clé pointe sur la meme chose
	// attente : nouvelle clé écrasée pointe sur nouvel element tandis que ancienne clé pointe sur la meme chose
	@Test
	public void ajouterDeuxCleUneValeurPuisEcraser() {
		try {
			Object A3 = new Object();
			Object A4 = new Object();
			Object B3 = new Object();
			Object B4 = new Object();
			this.C.ajouter(A3,B3);
			this.C.ajouter(A4,B3);
			this.C.ajouter(A4,B4);
			assertTrue(this.C.present(A3));
			assertTrue(this.C.present(A3));
			this.unmod_taille = false;
			assertTrue(this.C.taille() == 4);
			assertSame(this.C.valeur(A3),B3);
			assertSame(this.C.valeur(A4),B4);
		} catch ( Exception e) {
			fail();
		}
	}

	// objectif : ajouter un couple clé valeur ou clé = valeur
	// attendu : rien ne nous empeche dans le contrat de faire cela
	@Test
	public void ajouterEgal() {
		try {
			Object o = new Object();
			this.C.ajouter(o, o);
			assertSame(o, this.C.valeur(o));
			assertTrue(this.C.present(o));
			this.unmod_taille = false;
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : retirer un couple existant, retirer assez de couple jusqu'a être
	// vide
	// attendu : taille est inférieur de 1, le couple n'existe plus sauf la valeur
	// qui peut etre liée à une autre clé, au bout d'un certain nombre
	// de suppression le conteneur devient vide
	@Test
	public void retirerExiste() {
		try {
			this.C.retirer(A1);
			assertTrue(this.C.taille() == (this.old_taille - 1));
			assertFalse(this.C.present(A1));
			try {
				this.C.valeur(A1);
				fail();
			} catch (ErreurConteneur e) {
				assertTrue(this.C.taille() == (this.old_taille - 1));
			}
			this.C.retirer(A2);
			assertTrue(this.C.estVide());
			assertTrue(this.C.taille() == 0);
			this.unmod_A1 = false;
			this.unmod_A2 = false;
			this.unmod_taille = false;
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : retirer un couple non existant
	// attendu : rien ne se passe, la taille devraient rester la meme
	@Test
	public void retirerNonExiste() {
		try {
			Object o = new Object();
			this.C.retirer(o);
			try {
				this.C.valeur(o);
				fail();
			} catch (ErreurConteneur e) {
				;
			}
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : retirer une clé qui pointe sur une valeur qui est associée sur une
	// autre clé
	// attendu : pas d'erreur
	@Test
	public void retirerDoubleAssociation() {
		try {
			Object A3 = new Object();
			this.C.ajouter(A3, this.B1);
			this.C.retirer(A1); // A1 pointait vers B1
			assertSame(this.B1, this.C.valeur(A3));
			this.unmod_A1 = false;
			try {
				this.C.valeur(A1);
				fail();
			} catch (ErreurConteneur e) {
				;
			}
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : vérifier que tout a bien été supprimé apres une remise à 0, pas
	// plus que 2 raz consécutif
	// attendu : aucun object coonu n'est dans le conteneur, un 2e appel a raz
	// provoque une erreur
	@Test
	public void raz() {
		try {
			this.C.raz();
			try {
				this.C.valeur(A1);
				fail();
			} catch (ErreurConteneur e) {
				;
			}

			try {
				this.C.valeur(A2);
				fail();
			} catch (ErreurConteneur e) {
				;
			}

			try {
				this.C.raz();
				fail();
			} catch (ErreurConteneur e) {
				;
			}
			assertTrue(this.C.taille() == 0);
			this.unmod_A1 = false;
			this.unmod_A2 = false;
			this.unmod_taille = false;
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : verifier que les elements A1 et A2 sont dans le conteneur mais
	// qu'un element A3 n'y est pas
	// attendu : respectivement, true true false
	@Test
	public void present() {
		try {
			Object A3 = new Object();
			assertTrue(this.C.present(A1));
			assertTrue(this.C.present(A2));
			assertFalse(this.C.present(A3));
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : verifier l'egalité des retours de valeur et quand la clé n'existe
	// pas
	// attendu : le retour devrait etre égale ( meme adresse ) à la valeur du couple
	// clé valeur et une clé qui n'existe pas
	// declenche une exception
	@Test
	public void valeur() {
		try {
			Object A3 = new Object();
			assertSame(this.C.valeur(A1), B1);
			assertSame(this.C.valeur(A2), B2);
			try {
				this.C.valeur(A3);
				fail();
			} catch (ErreurConteneur e) {
				;
			}
		} catch (Exception e) {
			fail();
		}
	}

	// objectif : tester le bon fonctionnement de estVide
	// attendu : le conteneur ne devrait pas etre vide
	@Test
	public void nonVide() {
		assertFalse(this.C.estVide());
	}

	// objectif : verifier l'attribut capacite
	// attendu : 5
	@Test
	public void capacite() {
		assertTrue(this.C.capacite() == 5);
	}

	// objectif : verifier l'attribut taille
	// attendu : 2
	@Test
	public void taille() {
		assertTrue(this.C.taille() == 2);
	}

	// objectif : on verifie que seulement les elements qu'on a voulu modifié ont
	// été modifiés
	@After
	public void end() {
		try {
			assertNotNull(this.C);
			assertNotNull(this.A1);
			assertNotNull(this.A2);
			assertNotNull(this.B1);
			assertNotNull(this.B2);

			if (unmod_cap) {
				assertTrue(this.C.capacite() == this.old_cap);
			}

			if (unmod_taille) {
				assertTrue(this.C.taille() == this.old_taille);
			}

			if (unmod_A1) {
				assertSame(this.C.valeur(A1), this.B1);
			}

			if (unmod_A2) {
				assertSame(this.C.valeur(A2), this.B2);
			}
		} catch (Exception e) {
			fail();
		}

	}

}
